<?php

return [
    'db' => [
        'default' => [
            'driver' => 'mysql',
            'host' => '127.0.0.1',
            'dbname' => 'bashakkord',
            'user' => 'root',
            'password' => '123qwe'
        ]

    ],
    'errors' => [
        403 => '///403',
        404 => '///404',
    ],
    'mail' => [
        'method' => 'php',
//        'host' => 'smtp.gmail.com',
//        'port' => 465,
//        'secure' => 'ssl',
//        'auth' => [
//            'username' => 'bashakkord@gmail.com',
//            'password' => 'bashakkord102',
//        ],
//        'sender' => 'BashAkkord',
    ],
    'extensions' => [
        'jquery' => [],

        'ckeditor' => [
            'location' => 'local',
        ],
        'ckfinder' => [
            'autoload' => false,
        ],
    ],

];